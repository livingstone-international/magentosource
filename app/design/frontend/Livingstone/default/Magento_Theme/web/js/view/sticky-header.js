define(['jquery', 'matchMedia', 'domReady!'], function ($) {
    'use strict';

    $.widget('mage.stickyHeader', {
        /**
         * Default options
         */
        options: {
            media: "(max-width: 768px)",
            class: "active-sticky-header",
        },

        /**
         * This method constructs a new widget.
         * @private
         */
        _create: function () {
            var self = this,
                height = self.element.outerHeight(true);

            var check = function () {
                return $(window).scrollTop() > height;
            };

            var scrollHandler = function () {
                if ( $(window).scrollTop() > height ) {
                    $(document.body).addClass(self.options.class);
                } else {
                    $(document.body).removeClass(self.options.class);
                }
            };

            mediaCheck({
                media: this.options.media,
                entry: function () {
                    scrollHandler();
                    $(window).scroll(scrollHandler);
                },
                exit: function () {
                    $(document.body).removeClass(self.options.class);
                    $(window).off("scroll", scrollHandler);
                },
            });
        },
    });

    return $.mage.stickyHeader;
});