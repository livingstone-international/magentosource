/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
], function ($) {
    'use strict';

    return function (Component) {
        return Component.extend({
            /**
             * @override
             */                          
             initConfig: function () {
                this._super();
                this.isPasswordVisible = true;                          
                return this;
            }  
        });
    }
});