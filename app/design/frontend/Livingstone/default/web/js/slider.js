define(['jquery', 'slick'], function ($) {
    'use strict';

    $.widget('mage.slider', {
        /**
         * Default options
         */
        options: {
            slidesToShow: 5,
            slidesToScroll: 1,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        rows: 2,
                    }
                },
            ]
        },

        /**
         * This method constructs a new widget.
         * @private
         */
        _create: function () {
            this.element.slick(this.options);
        }
    });

    return $.mage.slider;
});