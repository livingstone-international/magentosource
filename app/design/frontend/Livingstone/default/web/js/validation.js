define([
    'jquery',
    'Magento_Ui/js/lib/validation/utils',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function ($, utils) {
    "use strict";

    return function () {
        $.validator.addMethod(
            'validate-zip-code',
            function (value) {
                return $.mage.isEmptyNoTrim(value) || /^\d{4}?$/g.test(value);
            },
            $.mage.__('Please enter a valid zip code.')
        );
        $.validator.addMethod(
            'validate-phone-number',
            function (value) {
                return $.mage.isEmptyNoTrim(value) || /^\+?\d+$/g.test(value);
            },
            $.mage.__('Please enter a valid phone number.')
        );
    }
});