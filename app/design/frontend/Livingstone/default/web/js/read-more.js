define(['jquery', 'mage/translate'], function ($) {
    'use strict';

    $.widget('mage.readMore', {
        /**
         * Default options
         */
        options: {
            class: "read-more",
            link: {
                text: $.mage.__("Read more"),
                attributes: {
                    class: "read-more-link",
                    href: "#",
                }
            }
        },

        /**
         * This method constructs a new widget.
         * @private
         */
        _create: function () {
            var $widget = this,
                $content = this.element;

            $("<a></a>", $widget.options.link.attributes)
                .text($widget.options.link.text)
                .click(function (event) {
                    event.preventDefault();
                    $content.removeClass($widget.options.class);
                    $(this).remove();
                    return false;
                })
                .insertAfter($content);

            $content.addClass($widget.options.class);
        }
    });

    return $.mage.readMore;
});