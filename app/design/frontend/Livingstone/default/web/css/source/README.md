Files in this folder will override corresponding files of `<parent_theme>/web/css/source/`.
The only exception for this rule is the `_extend.less` file which is merging the content of all nested themes.
Look Magento's documentation for [CSS and LESS preprocessing](https://devdocs.magento.com/guides/v2.2/frontend-dev-guide/css-topics/css-preprocess.html) for the reference.