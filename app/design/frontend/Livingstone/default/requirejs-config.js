var config = {
    map: {
        '*': {
            slider: 'js/slider',
            productSlider: 'js/productSlider',
            categorySlider: 'js/categorySlider',
            readMore: 'js/read-more',
            qtyControl: 'js/qty-control',
            catalogAddToCart: 'js/catalog-add-to-cart',
            customValidations: 'js/validation'
        }
    },
    paths: {
        slick: 'js/lib/slick/slick',
        matchHeight: 'js/lib/matchHeight/matchHeight',
    },
    shim: {
        slick: {
            deps: ['jquery']
        },
        matchHeight: {
            deps: ['jquery']
        },
    }
};